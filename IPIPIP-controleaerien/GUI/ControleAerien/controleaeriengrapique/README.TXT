Classes fournies:
==================

AvionGUI pour afficher un avion. Constructeur:
public AvionGUI(ActionsAvion acv)

CamionGUI pour afficher un camion. Constructeur:
public CamionGUI(ActionsCamion ac)

TimerGlobalGUI pour afficher un timer. Constructeur:
public TimerGlobalGUI(TimerDuJeu tdj)

méthodes que vous devez utiliser dans ces classes:
===================================================

CamionGUI:
    
    public void set_camion_numero(int numero);
    public void set_camion_etat(String etat);
    
AvionGUI:
    
    public void set_avion_numero(int numero);
    public void set_nombre_passagers(int nombre);
    public void set_etat_avion(String etat);
    public void set_date_arrivee(int date_arrivee);
    public void set_date_depart_prevue(int date_depart_prevue);
    public void set_retard(int retard);
    public void set_kerosene_actuel(int kerosene_actuel);
    public void set_kerosene_min(int kerosene_min);
    public void set_kerosene_max(int kerosene_max);


Interfaces que vous devez implémenter: 
=======================================

ActionsAvion (nécessaire à la construction du AvionGUI):

public interface ActionsAvion {
    
    public void decoller();
    public void atterrir(int piste);
    public void roulerversporte(int porte);
    public void roulerverspiste(int piste);
    
}

ActionsCamion (nécessaire à la construction du CamionGUI)

public interface ActionsCamion {
    
    public void remplir(int porte, int kerosene);
    
}

TimerDuJeu (nécessaire à la construction du TimerGlobalGUI)

public interface TimerDuJeu {
    
    public void appel_chaque_seconde();
    
}

