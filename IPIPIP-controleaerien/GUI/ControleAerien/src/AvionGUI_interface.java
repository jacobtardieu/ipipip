/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author sharrock
 */
public interface AvionGUI_interface {
    
    public void set_avion_numero(int numero);
    public void set_nombre_passagers(int nombre);
    public void set_etat_avion(String etat);
    public void set_date_arrivee(int date_arrivee);
    public void set_date_depart_prevue(int date_depart_prevue);
    public void set_retard(int retard);
    public void set_kerosene_actuel(int kerosene_actuel);
    public void set_kerosene_min(int kerosene_min);
    public void set_kerosene_max(int kerosene_max);
    
}
