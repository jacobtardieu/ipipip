
import java.io.*;
import java.util.ArrayList;

/**
 * Classe qui défninit l'architecture de l'aéroport.
 * Elle gèrera aussi la dimension monétaire qui n'est pas encore implémentée.
 * @author Jacob Tardieu
 *
 */
public class Aeroport implements Periodique {

	private int[] pistes;
	private ArrayList<Porte> portes;
	private ArrayList<MonCamion> camions;
	private ArrayList<MonAvion> avions;
	private TextGUI output;
	private MonTimer timer;
	private Etats recap;
	
	private int nbpasstransporte;
	private int litreskero;
	private int retard;

	/**
	 * Contructeur de l'architecture de l'aéroport. À voir pour passer avec SnakeYalm.
	 * Pour l'instant il n'y a que les avions qui sont contruits à partir du fichier texte, la contruction de l'aéroport en entier à partir du fichier
	 * texte est à venir...
	 * @param filename
	 * Le fichier texte sera contruit avec un entier par ligne selon la structure suivante :
	 * nombre de pistes  ;
	 * nombre de portes  ;
	 * nombre de camions  ;
	 * 
	 * "Avion i"
	 * kérosène présent  ;
	 * heure de décollage  ;
	 * heure d'atterissage  ;
	 * nombre de passagers à bord  ;
	 * 
	 * "Avion i+1"
	 * ...
	 */
	public Aeroport(String filename) {
		int nbpistes;
		int nbportes;
		int nbcamions;
		int numero=0;
		String ligne;
		int kero;	
		int hdeco;
		int hater;
		int nbpass;
		timer=new MonTimer();
		timer.enregistrePeriodique(this);
		new TimerGlobalGUI(timer);
		try {
			//lecture du début du fichier texte, en considérant qu'il est bien formé.
			BufferedReader aLire = new BufferedReader(new FileReader(filename));
			nbpistes=Integer.parseInt(aLire.readLine());
			nbportes=Integer.parseInt(aLire.readLine());
			nbcamions=Integer.parseInt(aLire.readLine());
			aLire.readLine();
			aLire.readLine();	//On saute deux lignes
			this.avions=new ArrayList<MonAvion>();
			this.camions=new ArrayList<MonCamion>();
			this.pistes=new int[nbpistes];
			this.portes=new ArrayList<Porte>();
			for (int i=0;  i<nbportes; i++) {
				portes.add(new Porte());
			}
			for (int i=0;i<nbpistes;i++) {
				pistes[i]=-1;
			}
			for (int i=0;i<nbcamions;i++) {
				camions.add(new MonCamion(this,timer,i));
			}	
			do {
				//lecture du fichier texte pour les avions
				ligne=aLire.readLine();
				if (ligne!=null) {
					kero=Integer.parseInt(ligne);
					nbpass=Integer.parseInt(aLire.readLine());
					hater=Integer.parseInt(aLire.readLine());
					hdeco=Integer.parseInt(aLire.readLine());
					
					
					aLire.readLine();
					aLire.readLine(); //On saute deux lignes
					//Construction de l'avion
					this.avions.add(new MonAvion(numero,kero,hdeco,hater,nbpass,timer,this));
					numero++;
				}
			}
			while (ligne!=null);
			aLire.close();//fermeture du fichier
		}
		catch (IOException e) {
			this.getOutput().afficher("Erreur dans la lecture du fichier définissant l'aéroport : " + e);
		}
		recap=new Etats();
		recap.setLocation(1500, 0);
		output=new TextGUI();
		output.setLocation(1200,400);
		this.nbpasstransporte=0;
		this.litreskero=0;
	}
	

	public int getPiste(int i) {
		return pistes[i];
	}
	public Porte getPorte(int i) {
		return portes.get(i);
	}
	public MonCamion getCamion(int i) {
		return camions.get(i);
	}
	public MonAvion getAvion(int i) {
		return avions.get(i);
	}
	public ArrayList<MonAvion> getAvion() {
		return avions;
	}
	public TextGUI getOutput() {
		return output;
	}
	public int nbPistes() {
		return pistes.length;
	}
	public int nbPortes() {
		return portes.size();
	}
	
	
	public void setPorte(int i, MonAvion a) {
		portes.get(i).setAvion(a);	
	}

	public void setPiste(int p, int a) {
		pistes[p]=a;
	}
	public void setAvion(int num, MonAvion avion) {
		avions.set(num, avion);
	}
	public void ajouterLitreskero(int l) {
		litreskero=litreskero+l;
	}
	public void ajouterNbpasstransporte(int n) {
		nbpasstransporte=nbpasstransporte+n;
	}
	public void ajouterRetard(int r) {
		retard=retard+r;
	}
	
	private void majRecap() {
		for (int i=0 ; i<pistes.length ; i++) {
			if (pistes[i]==-1) {
				recap.setPiste(i, "Libre");
			}
			else {
				recap.setPiste(i, "Avion "+pistes[i]);
			}
		}
		for (int i=0 ; i<portes.size() ; i++) {
			if (portes.get(i).getAvion()==null) {
				recap.setPorte(i,"Libre");
			}
			else {
				recap.setPorte(i, "Avion "+portes.get(i).getAvion().getNumero());
			}
		}
	}
	
	/**
	 * Méthode qui est appelée chaque seconde par le timer. Elle permet de détecter la fin du jeu.
	 */
	public void regulier() {
		majRecap();
		if (this.getAvion().isEmpty()) {
			this.getOutput().setVisible(false);
			this.recap.setVisible(false);
			timer.desenregistrePeriodique(this);
			new AuRevoir(nbpasstransporte,retard,litreskero, 2*nbpasstransporte - retard);
		}
	}

}
