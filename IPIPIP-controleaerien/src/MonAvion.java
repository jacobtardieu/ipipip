
/**
 * MonAvion est une classe qui représente l'objet avion.
 * @author Jacob Tardieu
 *
 */

public class MonAvion implements ActionsAvion, Periodique {

	private String etat;
	private String autorisation;

	private Aeroport cdg;
	private AvionGUI gui;

	private MonTimer timer;

	private int compte;
	private int numero;
	private int kero;
	private int keromin;
	private int keromax;
	private int hdeco;
	private int hater;
	private int nbpass;
	private int nbpassmax;
	private int piste;
	private int porte;

	private int retard;
	private int nbpasstransporte;
	private int litreskero;

	private boolean debarqpass;
	private boolean embarqpass;
	private boolean remplissage;
	private boolean ok;

	/**
	 * Constructeur de l'avion. Est appelé par l'aéroport après lecture du fichier de niveau.
	 * @param numero
	 * Le numéro de l'avion
	 * @param kero
	 * Le kérosène actuel
	 * @param hdeco
	 * L'heure de décollage prévue
	 * @param hater
	 * L'heure d'atterissage prévue
	 * @param nbpass
	 * Le nombre de passagers à bord
	 * @param timer
	 * Le timer global qui sert pour perdre du temps
	 * @param cdg
	 * L'aéroport auquel appartient l'avion
	 */
	public MonAvion(int numero, int kero, int hdeco, int hater, int nbpass, MonTimer timer, Aeroport cdg) {
		this.autorisation="";
		this.cdg=cdg;
		this.numero=numero;
		this.kero=kero;	
		this.hdeco=hdeco;
		this.hater=hater;
		this.nbpass=nbpass;
		this.nbpassmax=nbpass;
		this.piste=-1;
		this.porte=-1;
		this.etat="En vol";
		this.timer=timer;
		this.retard=0;
		this.keromin=20;
		this.keromax=1000;
		this.nbpasstransporte=0;
		this.litreskero=0;
		this.debarqpass=false;
		this.embarqpass=false;
		this.ok=false;
		timer.enregistrePeriodique(this);
		this.gui=new AvionGUI(this);
		gui.setLocation(300, numero*270);
		gui.setTitle("Avion "+numero);
	}

	//Getters
	public int getNumero() {
		return numero;
	}	
	public String getEtat(int i) {
		return etat;
	}	
	public int getKero() {
		return kero;
	}
	public int getKeromax() {
		return keromax;
	}
	public int getHdeco() {
		return hdeco;
	}
	public int getHater() {
		return hater;
	}
	public int getNbpass() {
		return nbpass;
	}
	public AvionGUI getGui() {
		return gui;
	}
	public boolean getRemplissage() {
		return remplissage;
	}

	//Setters
	public void setNumero(int numero) {
		this.numero=numero;
		gui.set_avion_numero(numero);
	}
	public void setKero(int kero) {
		this.kero=kero;
	}
	public void setKeromax(int keromax) {
		this.keromax=keromax;
	}
	public void setHdeco(int hdeco) {
		this.hdeco=hdeco;
	}
	public void setHater(int hater) {
		this.hater=hater;
	}
	public void setNbpass(int nbpass) {
		this.nbpass=nbpass;
	}
	public void setRemplissage(boolean b) {
		this.remplissage=b;
	}
	public void ajouterKero(int i) {
		this.litreskero=this.litreskero+i;
	}


	/**
	 * Méthode qui définit les paramètres de l'avion qui sont actualisés chaque seconde. Elle est appelée chaque seconde par le timer.
	 */
	public void regulier() {
		consoKero();
		compte++;
		this.changegui();
		calculRetard();
		gameover();
		affichage();
		debarquementPassagers();
		embarquementPassagers();
		majAutorisation();
		actionDecoller();
		actionAtterir();
		actionRoulerversporte();
		actionRoulerverspiste();
	}

	/**
	 * Méthode qui affiche l'avion quand il approche et qui gère sa diparition de l'écran (en envoyant préalablement les infos à l'aéroport).
	 */
	private void affichage() {
		if (timer.getSeconde()>hater-60 && timer.getSeconde()< hdeco && !this.gui.isVisible()) {
			this.gui.setVisible(true);
			cdg.getOutput().afficher("L'avion "+numero+" est apparu sur votre écran de contrôle.");
		}
		if (ok && etat=="En vol" && timer.getSeconde()> hdeco+30) {
			cdg.getOutput().afficher("L'avion "+numero+" a disparu de votre écran de contrôle.");
			timer.desenregistrePeriodique(this);
			cdg.ajouterLitreskero(litreskero);
			cdg.ajouterNbpasstransporte(nbpasstransporte);
			cdg.ajouterRetard(retard);
			gui.setVisible(false);
			cdg.getAvion().remove(this);
		}
		if (etat!="En vol" && timer.getSeconde()>hdeco+30) {
			timer.desenregistrePeriodique(this);
			this.gui.setVisible(false);
			cdg.getOutput().afficher("L'avion "+numero+" n'a pas pu décoller à temps ! Gros malus !");
			cdg.ajouterRetard(this.retard);
			cdg.getAvion().remove(this);
		}
		if (etat=="En vol" && timer.getSeconde()>hater+30 && timer.getSeconde()<hdeco-30) {
			timer.desenregistrePeriodique(this);
			this.gui.setVisible(false);
			cdg.getOutput().afficher("L'avion "+numero+" n'a pas pu atterir à temps ! Gros malus !");
			cdg.ajouterRetard(this.retard);
			cdg.getAvion().remove(this);
		}


	}


	/**
	 * Méthode qui fait varier la consommation de kérosène en fonction de l'état de l'aéronef.
	 */
	private void consoKero() {
		if (etat=="En vol") {
			kero=kero - 5;
		}
		if (etat=="Atterissage piste" || etat=="Sur la piste" || etat=="Roulage vers piste" || etat=="Roulage vers porte") {
			kero=kero -1;
		}
		if (etat=="Décollage piste") {
			kero=kero -10;
		}
	}

	/**
	 * Méthode mettant à jour le champ autorisation en fonction du temps et de l'état de l'avion
	 */
	private void majAutorisation() {
		if (embarqpass) {
			if (timer.getSeconde()%3==0) {
				autorisation="Embarquement passagers.";
			}
			if (timer.getSeconde()%3==1) {
				autorisation="Embarquement passagers..";
			}
			if (timer.getSeconde()%3==2) {
				autorisation="Embarquement passagers...";
			}
		}
		
		if (debarqpass) {
			if (timer.getSeconde()%3==0) {
				autorisation="Débarquement passagers.";
			}
			if (timer.getSeconde()%3==1) {
				autorisation="Débarquement passagers..";
			}
			if (timer.getSeconde()%3==2) {
				autorisation="Débarquement passagers...";
			}
		}
		if (!embarqpass && !debarqpass) {
			autorisation="";
		}
		
		if (etat=="En vol" && timer.getSeconde() < hdeco - 30) {
			if (timer.getSeconde() < hdeco - 30) {
				if (timer.getSeconde() > hater - 30) {
					autorisation="Autorisation atterissage accordée";
				}
				else {
					autorisation="Attente autorisation atterissage";
				}
			}
			else {
				autorisation="";
			}
		}
		if (etat=="Sur la piste") {
			if (timer.getSeconde() > hater +30) {
				if (timer.getSeconde() > hdeco -30) {
					autorisation="Autorisation décollage accordée";

				}
				else {
					autorisation="Attente autorisation décollage";
				}
			}
			else {
				autorisation="";
			}
		}
	}

	/**
	 * Méthode qui calcule le retard de l'avion en fonction du temps et de l'état
	 */
	private void calculRetard() {
		if (etat=="En vol" && timer.getSeconde()>hater &&timer.getSeconde() < hdeco-30) {
			retard++;
		}
		if (etat!="En vol" && timer.getSeconde()>hdeco) {
			retard++;
		}
	}

	/**
	 * Méthode qui vérifie que les avions ne se crashent pas
	 */
	private void gameover() {
		if (kero<keromin) {
			for (Periodique p : cdg.getAvion()) {
				timer.desenregistrePeriodique(p);	// Arrêt total du jeu
			}
			new GameOver(numero);
		}
	}

	/**
	 * Méthode qui débarque les passagers et qui lance l'embarquement juste après
	 */
	private void debarquementPassagers() {
		if (debarqpass && etat=="À la porte") {
			if (nbpass>=8) {
				nbpass = nbpass - 8;
				nbpasstransporte=nbpasstransporte+8;
			}
			else {
				nbpasstransporte=nbpasstransporte+ nbpass;
				nbpass = 0;
				debarqpass=false;
				embarqpass=true;
			}
		}
	}

	/**
	 * Méthode qui embarque les passagers. Elle est appelée par la méthode debarquementPassagers().
	 */
	private void embarquementPassagers() {
		if (embarqpass) {
			if (nbpass + 8 <= nbpassmax) {
				nbpass = nbpass + 8;
				nbpasstransporte=nbpasstransporte+16;
			}
			else {
				nbpasstransporte=nbpasstransporte+ (nbpassmax-nbpass);
				nbpass = nbpassmax;
				embarqpass=false;
				ok=true;
			}
		}
	}


	/**
	 * Méthode qui met à jour l'interface graphique chaque seconde.
	 */
	public void changegui() {
		gui.set_avion_numero(numero);
		gui.set_kerosene_actuel(kero);
		gui.set_date_depart_prevue(hdeco);
		gui.set_date_arrivee(hater);
		gui.set_nombre_passagers(nbpass);
		gui.set_etat_avion(afficheEtat());
		gui.set_retard(retard);
		gui.set_kerosene_max(keromax);
		gui.set_kerosene_min(keromin);
		gui.set_autorisation(autorisation);
	}

	/**
	 * Méthode ajoutant le numéro de l'éventuelle piste ou porte sur laquelle l'avion est
	 * @return
	 * L'état de l'avion plus éventuellement le numéro de la piste ou porte et l'inscription indiquant le remplissage.
	 */
	private String afficheEtat() {
		if (porte!=-1) {
			if (remplissage) {
				if (timer.getSeconde()%3==0) {
					return etat+" "+porte+"  -   Remplissage en cours.";
				}
				if (timer.getSeconde()%3==1) {
					return etat+" "+porte+"  -   Remplissage en cours..";
				}
				if (timer.getSeconde()%3==2) {
					return etat+" "+porte+"  -   Remplissage en cours...";
				}
			}
			else {
				return etat+" "+porte;
			}
		}
		if (piste!=-1) {
			return etat+" "+piste;
		}
		return etat;
	}



	/**
	 * Décollage de l'avion. 
	 */
	public void decoller() {
		if (this.piste!=-1 && etat=="Sur la piste") {
			if (autorisation=="Autorisation décollage accordée") {
				compte=0;
				etat="Décollage piste";
				autorisation="";
				cdg.getOutput().afficher("AVION "+numero+" - Décollage piste "+this.piste);
				cdg.setPiste(this.piste,-1);
			}
			else {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du décollage : pas d'autorisation.");
			}
		}
		else {
			if (etat=="En vol") {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du décollage : aéronef déjà en vol.");
			}
			else {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du décollage : l'aéronef n'est pas sur une piste.");
			}
		}
	}

	/**
	 * Méthode qui permet de prendre du temps pour faire décoller l'avion.
	 * Elle est appelée chaque seconde, et c'est la méthode decoller() qui change l'état qui déclenche cette méthode.
	 */
	private void actionDecoller() {
		if (etat=="Décollage piste" && compte==5) {
			etat="En vol";
			this.piste=-1;
			if (ok) {
				int benef = 2*(nbpasstransporte) + litreskero - retard;
				cdg.getOutput().afficher("Bravo, l'avion "+numero+" a décollé avec un retard de "+retard+" minutes et un bénéfice de "+benef+" euros.");
			}
		}
	}

	/**
	 * Atterrissage de l'avion.
	 * @param piste
	 * Numéro de la piste sur laquelle on désire atterrir.
	 */
	public void atterrir(int piste) {
		if (piste>=0 && piste<cdg.nbPistes()) {
			if (etat=="En vol") {
				if(autorisation=="Autorisation atterissage accordée") {
					if (cdg.getPiste(piste) ==-1) {
						compte=0;
						etat="Atterissage piste";
						this.piste=piste;
						cdg.setPiste(piste, this.getNumero());
						cdg.getOutput().afficher("AVION "+numero+" - Atterissage piste "+this.piste);
					}
					else {
						cdg.getOutput().afficher("AVION "+numero+" - Erreur lors de l'atterissage : piste "+piste+" occupée !");
					}
				}
				else {
					cdg.getOutput().afficher("AVION "+numero+" - Erreur lors de l'atterissage : pas d'autorisation.");
				}
			}
			else {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors de l'atterissage : l'aéronef n'est pas en vol !");
			}
		}
		else {
			cdg.getOutput().afficher("AVION "+numero+" - Erreur lors de l'atterissage : la piste "+piste+" n'existe pas !");
		}
	}

	/**
	 * Prend du temps pour faire atterir l'avion.
	 * Même principe que actionDecoller().
	 */
	private void actionAtterir() {
		if (etat=="Atterissage piste" && compte==5) {
			etat="Sur la piste";
		}
	}

	/**
	 * Roulage de l'avion vers la porte.
	 * @param porte
	 * Numéro de la porte où l'on veut aller.
	 */
	public void roulerversporte(int porte) {
		if (porte >=0 && porte<cdg.nbPortes()) {
			if (this.etat=="Sur la piste") {
				if (cdg.getPorte(porte).getAvion()==null) {
					compte=0;
					cdg.setPiste(this.piste, -1);
					this.porte=porte;
					this.piste=-1;
					cdg.setPorte(porte, this);
					etat="Roulage vers porte";
					cdg.getOutput().afficher("AVION "+numero+" - Roulage vers porte "+porte);

				}
				else {
					cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage : porte "+porte+" occupée !");
				}
			}
			else {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage : l'aéronef n'est pas sur une piste !");
			}
		}
		else {
			cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage : la porte "+porte+" n'existe pas !");
		}
	}

	/**
	 * Même principe que les autres méthodes action@@@().
	 */
	private void actionRoulerversporte() {
		if (etat=="Roulage vers porte" && compte==5) {
			etat="À la porte";
			debarqpass=true;
		}
	}

	/**
	 * Roulage de l'avion vers la piste.
	 * @param piste
	 * Numéro de la piste vers laquelle on veut rouler.
	 */
	public void roulerverspiste(int piste) {
		if (piste >= 0 && piste<cdg.nbPistes()) {
			if (!remplissage) {
				if (!embarqpass && !debarqpass) {
					if (etat!="En vol") {
						if (cdg.getPiste(piste) ==-1) {
							compte=0;
							this.piste=piste;
							cdg.getOutput().afficher("AVION "+numero+" - Roulage vers piste "+piste);
							cdg.setPiste(piste, this.getNumero());
							etat="Roulage vers piste";
							if (this.porte!=-1) {
								cdg.setPorte(porte, null);
								this.porte=-1;
							}
						}
						else {
							cdg.getOutput().afficher("AVION "+numero+" Erreur lors du roulage vers piste : piste "+piste+" occupée !");
						}
					}
					else {
						cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage vers piste : l'aéronef est en vol !");
					}
				}
				else {
					cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage vers piste : les passagers sont en train de monter ou descendre.");
				}
			}
			else {
				cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage vers piste : l'aéronef est en remplissage !");
			}
		}
		else {
			cdg.getOutput().afficher("AVION "+numero+" - Erreur lors du roulage vers piste : la piste "+piste+" n'existe pas !");
		}

	}

	/**
	 * Même méthode que les autres actions@@@()
	 */
	private void actionRoulerverspiste() {
		if (etat=="Roulage vers piste" && compte==5) {
			etat="Sur la piste";
		}
	}

}
