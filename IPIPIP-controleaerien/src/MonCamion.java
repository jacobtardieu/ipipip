
/**
 * MonCamion est une classe qui représente l'objet Camion.
 * @author Jacob Tardieu
 *
 */
public class MonCamion implements ActionsCamion, Periodique {

	private Aeroport aeroport;
	private String etat;
	private CamionGUI gui;
	private int numero;
	private int aRemplir;
	private MonAvion av;

	/**
	 * Constructeur de l'objet MonCamion. Il est appelé par la classe aéroport qui construit le nombre de camion demandé dans le fichier de niveau
	 * @param aeroport
	 * L'aéroport auquel appartient le camion.
	 * @param timer
	 * Le timer qui sert pour perdre du temps
	 * @param numero
	 * Le numéro du camion
	 */
	public MonCamion(Aeroport aeroport, MonTimer timer, int numero) {
		this.aeroport=aeroport;
		this.gui=new CamionGUI(this);
		this.etat="Libre";
		this.numero=numero;
		this.gui.set_camion_numero(numero);
		timer.enregistrePeriodique(this);
		gui.setLocation(1000, numero*120);
		gui.setTitle("Camion "+numero);
	}

	@Override
	/**
	 * Méthode qui remplit un avion
	 * @param porte
	 * Numéro de la porte à laquelle on veut aller remplir
	 * @param kerosene
	 * La quantité de kérosène que l'on veut mettre dedans
	 */
	public void remplir(int porte, int kerosene) {
		if (kerosene >=0) {
			if (etat=="Libre") {
				if (porte >=0 && porte<aeroport.nbPortes()) {
					if (aeroport.getPorte(porte).getAvion() != null) {
						av=aeroport.getPorte(porte).getAvion();
						if (!av.getRemplissage()) {
							if (av.getKero()+kerosene <= av.getKeromax()) {
								etat="Occupé";
								this.aRemplir=kerosene;
								av.setRemplissage(true);
							}
							else {
								aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : la quantité demandé ne rentre pas dans le réservoir de l'aéronef.");
							}
						}
						else {
							aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : l'aéronef "+av.getNumero()+" est déjà en ravitaillement.");
						}
					}
					else {
						aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : pas d'avion à cette porte !");
					}
				}
				else {
					aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : la porte n'existe pas.");
				}
			}
			else {
				aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : camion occupé.");
			}
		}
		else {
			aeroport.getOutput().afficher("CAMION "+numero+" - Erreur lors du remplissage : ne faites pas les malins avec des nombres négatifs !");
		}
	}

	/**
	 * Méthode qui permet de prendre du temps au remplissage de l'avion.
	 */
	private void actionRemplir() {
		if (etat=="Occupé" && av.getKero() <= av.getKeromax()) {
			if (aRemplir > 8) {
				av.setKero(av.getKero()+8);
				aRemplir=aRemplir-8;
				av.ajouterKero(8);
			}
			else {
				av.setKero(av.getKero()+aRemplir);
				av.ajouterKero(aRemplir);
				aRemplir=0;
				etat="Libre";
				av.setRemplissage(false);
			}
		}
	}

	@Override
	public void regulier() {
		gui.set_camion_etat(etat);
		actionRemplir();
	}

}


