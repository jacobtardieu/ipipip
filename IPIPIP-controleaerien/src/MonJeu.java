
/**
 * Classe servant uniquement à appeler le GUI du niveau qui se chargera ensuite du reste.
 * @author Jacob Tardieu
 *
 */
public class MonJeu {


	public static void main(String[] args) {
		new Niveau();
	}

}
