
import java.util.HashSet;
import java.util.Set;

/**
 * Classe définissant le timer
 * @author Jacob Tardieu
 *
 */
public class MonTimer implements TimerDuJeu {

	private int seconde =1;

	private Set<Periodique> aRappeler = new HashSet<Periodique>();
	
	
	public void enregistrePeriodique(Periodique periodique) {
		aRappeler.add(periodique);
	}

	public void desenregistrePeriodique(Periodique periodique) {
		aRappeler.remove(periodique);
	}

	/**
	 * Méthode étant appelée chaque seconde par TimerGlobalGUI.
	 */
	public void appel_chaque_seconde() {
		seconde++;
			for (Periodique periodique : new HashSet<Periodique>(aRappeler)) {
				periodique.regulier();
		}
	}

	public int getSeconde() {
		return seconde;
	}
	
	public Set<Periodique> getARappeler() {
		return aRappeler;
	}
	
	public void setSeconde(int seconde) {
		this.seconde = seconde;
	}


}