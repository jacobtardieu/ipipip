
/**
 * Classe qui définit une porte
 * @author Jacob Tardieu
 *
 */
public class Porte {

	private MonAvion avion;
	
	public Porte(MonAvion avion) {
		this.avion=avion;
	}
	
	public Porte() {
		this(null);
	}

	public MonAvion getAvion() {
		return avion;
	}

	public void setAvion(MonAvion avion) {
		this.avion = avion;
	}
	
}
